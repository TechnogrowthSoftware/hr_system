import { OfferLetterComponent } from './gl_offerletter/offer-letter/offer-letter.component';
import { ProbationExtensionletterComponent } from './gl_offerletter/probation-extensionletter/probation-extensionletter.component';
import { HolidayCalendarComponent } from './leavesAttendance/holiday-calendar/holiday-calendar.component';
import { ViewLeaveApprovalComponent } from './leavesAttendance/view-leave-approval/view-leave-approval.component';
import { OtherLabouractsComponent } from './it_incomeTax/other-labouracts/other-labouracts.component';
import { EsicComponent } from './it_incomeTax/esic/esic.component';
import { IncreamentPromotionletterComponent } from './gl_offerletter/increament-promotionletter/increament-promotionletter.component';
import { ConfirmationletterComponent } from './gl_offerletter/confirmationletter/confirmationletter.component';
import { AppointmentletterComponent } from './gl_offerletter/appointmentletter/appointmentletter.component';
import { LeavePolicyComponent } from './cp_leavePolicy/leave-policy/leave-policy.component';
import { HierarchyComponent } from './md_employee_master/hierarchy/hierarchy.component';
import { LocationMasterComponent } from './md_employee_master/location-master/location-master.component';
import { DesignationMasterComponent } from './md_employee_master/designation-master/designation-master.component';
import { DepartmentMasterComponent } from './md_employee_master/department-master/department-master.component';
import { EmployeeMasterComponent } from './md_employee_master/employee-master/employee-master.component';
import { HrLoginComponent } from './hr-login/hr-login.component';
import { HrMenuComponent } from './hr-menu/hr-menu.component';
import { HrDashboardComponent } from './hr-dashboard/hr-dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalaryComponent } from './salary/salary.component';
import { ExperienceletterComponent } from './gl_offerletter/experienceletter/experienceletter.component';
import { RelievingletterComponent } from './gl_offerletter/relievingletter/relievingletter.component';
import { IncomeTaxComponent } from './it_incomeTax/income-tax/income-tax.component';
import { PfComponent } from './it_incomeTax/pf/pf.component';
import { PtComponent } from './it_incomeTax/pt/pt.component';
import { RequestoravailcompoffComponent } from './leavesAttendance/requestoravailcompoff/requestoravailcompoff.component';
import { LeaveSummaryComponent } from './leavesAttendance/leave-summary/leave-summary.component';
import { EmployeeLeaveHistoryComponent } from './leavesAttendance/employee-leave-history/employee-leave-history.component';
import { AttendanceRegularComponent } from './leavesAttendance/attendance-regular/attendance-regular.component';
import { SalaryMasterComponent } from './md_employee_master/salary-master/salary-master.component';
import { LeaveAndHolidayMasterComponent } from './md_employee_master/leave-and-holiday-master/leave-and-holiday-master.component';

const routes: Routes = [

  { path:'',redirectTo:'/login',pathMatch:'full'},
  { path : 'login', component :HrLoginComponent},
  { path:'menu',component:HrMenuComponent, 
                                        children:[
                                                       
                                                        { path:'hrdashboard', component:HrDashboardComponent},
                                                        // EmployeeMaster Routes
                                                        { path:'employeemaster',component:EmployeeMasterComponent},
                                                        { path:'departmentmaster',component:DepartmentMasterComponent},
                                                        { path:'designationmaster',component:DesignationMasterComponent},
                                                        { path:'locatiomaster',component:LocationMasterComponent},
                                                        { path:'hiearchy',component:HierarchyComponent},
                                                        { path:'salarymaster',component:SalaryMasterComponent},
                                                        { path:'leavesmaster',component:LeaveAndHolidayMasterComponent},

                                                        // Salary Routes

                                                        { path:'salary',component:SalaryComponent},

                                                        // Company Policies Routes

                                                        { path:'leavepolicy',component:LeavePolicyComponent},

                                                        // Letters Routes

                                                        { path:'appointmentletter',component:AppointmentletterComponent},
                                                        { path:'confirmationletter',component:ConfirmationletterComponent},
                                                        { path:'experienceletter',component:ExperienceletterComponent},
                                                        { path:'increamnetletter',component:IncreamentPromotionletterComponent},
                                                        { path:'offerletter',component:OfferLetterComponent},
                                                        { path:'relievingletter',component:RelievingletterComponent},
                                                        { path:'probationextension',component:ProbationExtensionletterComponent},

                                                        // pfEscisIt

                                                        { path:'esic',component:EsicComponent},
                                                        { path:'proofofinvestment',component:IncomeTaxComponent},
                                                        { path:'otheracts',component:OtherLabouractsComponent},
                                                        { path:'pf',component:PfComponent},
                                                        { path:'pt',component:PtComponent},

                                                        // Leaves

                                                        { path:'viewLeave',component:ViewLeaveApprovalComponent},
                                                        { path:'requestcompoff',component:RequestoravailcompoffComponent},
                                                        { path:'leavesummary',component:LeaveSummaryComponent},
                                                        { path:'holidaycalendar',component:HolidayCalendarComponent},
                                                        { path:'employeeleave',component:EmployeeLeaveHistoryComponent},
                                                        { path:'attendanceRegualries',component:AttendanceRegularComponent}

  
                                                ]},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
