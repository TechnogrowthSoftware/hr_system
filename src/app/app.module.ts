
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HrDashboardComponent } from './hr-dashboard/hr-dashboard.component';
import { HrMenuComponent } from './hr-menu/hr-menu.component';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CalendarModule } from '@progress/kendo-angular-dateinputs';
import { HrLoginComponent } from './hr-login/hr-login.component';
import { DesignationMasterComponent } from './md_employee_master/designation-master/designation-master.component';
import { LocationMasterComponent } from './md_employee_master/location-master/location-master.component';
import { DepartmentMasterComponent } from './md_employee_master/department-master/department-master.component';
import { HierarchyComponent } from './md_employee_master/hierarchy/hierarchy.component';
import { AppointmentletterComponent } from './gl_offerletter/appointmentletter/appointmentletter.component';
import { ConfirmationletterComponent } from './gl_offerletter/confirmationletter/confirmationletter.component';
import { ProbationExtensionletterComponent } from './gl_offerletter/probation-extensionletter/probation-extensionletter.component';
import { IncreamentPromotionletterComponent } from './gl_offerletter/increament-promotionletter/increament-promotionletter.component';
import { ExperienceletterComponent } from './gl_offerletter/experienceletter/experienceletter.component';
import { RelievingletterComponent } from './gl_offerletter/relievingletter/relievingletter.component';
import { SalaryComponent } from './salary/salary.component';
import { PfComponent } from './it_incomeTax/pf/pf.component';
import { EsicComponent } from './it_incomeTax/esic/esic.component';
import { PtComponent } from './it_incomeTax/pt/pt.component';
import { OtherLabouractsComponent } from './it_incomeTax/other-labouracts/other-labouracts.component';
import { EmployeeMasterComponent } from './md_employee_master/employee-master/employee-master.component';
import { LeavePolicyComponent } from './cp_leavePolicy/leave-policy/leave-policy.component';
import { OfferLetterComponent } from './gl_offerletter/offer-letter/offer-letter.component';
import { IncomeTaxComponent } from './it_incomeTax/income-tax/income-tax.component';
import { ViewLeaveApprovalComponent } from './leavesAttendance/view-leave-approval/view-leave-approval.component';
import { EmployeeLeaveHistoryComponent } from './leavesAttendance/employee-leave-history/employee-leave-history.component';
import { LeaveSummaryComponent } from './leavesAttendance/leave-summary/leave-summary.component';
import { HolidayCalendarComponent } from './leavesAttendance/holiday-calendar/holiday-calendar.component';
import { RequestoravailcompoffComponent } from './leavesAttendance/requestoravailcompoff/requestoravailcompoff.component';
import { AttendanceRegularComponent } from './leavesAttendance/attendance-regular/attendance-regular.component';
import { UploadModule } from '@progress/kendo-angular-upload';
import { HttpClientModule } from '@angular/common/http';
import { PopupModule } from '@progress/kendo-angular-popup';
import { ReactiveFormsModule} from '@angular/forms';

//Material 

import {MatTableModule} from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { SalaryMasterComponent } from './md_employee_master/salary-master/salary-master.component';
import { LeaveAndHolidayMasterComponent } from './md_employee_master/leave-and-holiday-master/leave-and-holiday-master.component';






@NgModule({
  declarations: [
    AppComponent,
    HrDashboardComponent,
    HrMenuComponent,
    HrLoginComponent,
    DesignationMasterComponent,
    LocationMasterComponent,
    DepartmentMasterComponent,
    HierarchyComponent,
    AppointmentletterComponent,
    ConfirmationletterComponent,
    ProbationExtensionletterComponent,
    IncreamentPromotionletterComponent,
    ExperienceletterComponent,
    RelievingletterComponent,
    SalaryComponent,
    PfComponent,
    EsicComponent,
    PtComponent,
    OtherLabouractsComponent,
    EmployeeMasterComponent,
    LeavePolicyComponent,
    OfferLetterComponent,
    IncomeTaxComponent,
    ViewLeaveApprovalComponent,
    EmployeeLeaveHistoryComponent,
    LeaveSummaryComponent,
    HolidayCalendarComponent,
    RequestoravailcompoffComponent,
    AttendanceRegularComponent,
    SalaryMasterComponent,
    LeaveAndHolidayMasterComponent,
   
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DateInputsModule,
    BrowserAnimationsModule,
    CalendarModule,
    FormsModule,
    UploadModule,
    HttpClientModule,
    PopupModule,
    ReactiveFormsModule,
    MatTableModule,
    MatInputModule,
    MatAutocompleteModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
