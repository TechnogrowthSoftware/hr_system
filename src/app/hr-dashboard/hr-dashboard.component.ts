import { Component, OnInit } from '@angular/core';
import { IntlService } from '@progress/kendo-angular-intl';

@Component({
  selector: 'app-hr-dashboard',
  templateUrl: './hr-dashboard.component.html',
  styleUrls: ['./hr-dashboard.component.css']
})
export class HrDashboardComponent implements OnInit {

  
  public model: any;
  noteList = new Array();
  note : any;
  strike : boolean;

  public show:boolean = false;
  public buttonName:any = 'View More';

  constructor(private intl: IntlService) {
    this.strike = false;
    //this.model = { dob : this.now};
    this.model = {dob : new Date('Mon Oct 29 2018 22:31:49 GMT+0530 (India Standard Time)')};
    //this.model = { dob: new Date() };
  }

  public ngOnInit() {
    console.log(this.model);
  }


  toggle() {
    this.show = !this.show;

    if(this.show)  
      this.buttonName = "View Less";
    else
      this.buttonName = "View More";
  }

  pushtolist(){
    let noteToAdd = { dash : this.strike,note : this.note};
    this.noteList.push(noteToAdd);
    console.log(this.noteList);
    this.note = '';
    noteToAdd = { dash : false,note : ''};
  }
  setStrikeOnLine(index : number){
    //this.noteList[index].dash = !this.noteList[index].dash;
    console.log(index)
    console.log(this.noteList[index]);
  }

}
