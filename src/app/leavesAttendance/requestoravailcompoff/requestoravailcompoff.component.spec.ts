import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestoravailcompoffComponent } from './requestoravailcompoff.component';

describe('RequestoravailcompoffComponent', () => {
  let component: RequestoravailcompoffComponent;
  let fixture: ComponentFixture<RequestoravailcompoffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestoravailcompoffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestoravailcompoffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
