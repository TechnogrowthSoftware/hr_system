import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewLeaveApprovalComponent } from './view-leave-approval.component';

describe('ViewLeaveApprovalComponent', () => {
  let component: ViewLeaveApprovalComponent;
  let fixture: ComponentFixture<ViewLeaveApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewLeaveApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewLeaveApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
