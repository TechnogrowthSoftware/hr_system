import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherLabouractsComponent } from './other-labouracts.component';

describe('OtherLabouractsComponent', () => {
  let component: OtherLabouractsComponent;
  let fixture: ComponentFixture<OtherLabouractsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherLabouractsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherLabouractsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
