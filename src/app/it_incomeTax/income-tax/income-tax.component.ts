import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-income-tax',
  templateUrl: './income-tax.component.html',
  styleUrls: ['./income-tax.component.css']
})
export class IncomeTaxComponent implements OnInit {
show=true;
showReason=true;
showEmployee=true;
  constructor() { }

  ngOnInit() {
  }

  toggleShow(){
    this.show = !this.show;
  }

  toggleReason(){
    this.showReason = !this.showReason;
  }

  toggleEmp () {
    this.showEmployee = !this.showEmployee;
  }
}
