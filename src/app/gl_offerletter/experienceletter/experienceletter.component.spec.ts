import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExperienceletterComponent } from './experienceletter.component';

describe('ExperienceletterComponent', () => {
  let component: ExperienceletterComponent;
  let fixture: ComponentFixture<ExperienceletterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExperienceletterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExperienceletterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
