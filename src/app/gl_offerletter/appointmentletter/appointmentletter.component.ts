import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-appointmentletter',
  templateUrl: './appointmentletter.component.html',
  styleUrls: ['./appointmentletter.component.css']
})
export class AppointmentletterComponent implements OnInit {
show=true;
showGen=true;
disp: boolean = true;
  constructor() { }

  ngOnInit() {
  }
  toggleShow(){
    this.show=!this.show;
  }

  showGenerate(){
    this.showGen = !this.showGen;
  }

  public onToggle(): void {
    this.disp = !this.disp;
}


  


}
