import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-confirmationletter',
  templateUrl: './confirmationletter.component.html',
  styleUrls: ['./confirmationletter.component.css']
})
export class ConfirmationletterComponent implements OnInit {
  show=true;
  showGen=true;
  constructor() { }

  ngOnInit() {
  }
  toggleShow(){
    this.show=!this.show;
  }

  showGenerate(){
    this.showGen = !this.showGen;
  }
}
