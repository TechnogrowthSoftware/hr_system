import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-increament-promotionletter',
  templateUrl: './increament-promotionletter.component.html',
  styleUrls: ['./increament-promotionletter.component.css']
})
export class IncreamentPromotionletterComponent implements OnInit {
  show=true;
  showGen=true;
  disp: boolean = true;
  constructor() { }

  ngOnInit() {
  }
  toggleShow(){
    this.show=!this.show;
  }

  showGenerate(){
    this.showGen = !this.showGen;
  }

  public onToggle(): void {
    this.disp = !this.disp;
}
}
