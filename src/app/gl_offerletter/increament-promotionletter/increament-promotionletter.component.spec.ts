import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncreamentPromotionletterComponent } from './increament-promotionletter.component';

describe('IncreamentPromotionletterComponent', () => {
  let component: IncreamentPromotionletterComponent;
  let fixture: ComponentFixture<IncreamentPromotionletterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncreamentPromotionletterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncreamentPromotionletterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
