import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-relievingletter',
  templateUrl: './relievingletter.component.html',
  styleUrls: ['./relievingletter.component.css']
})
export class RelievingletterComponent implements OnInit {
  show=true;
  showGen=true;
  disp: boolean = true;
  constructor() { }

  ngOnInit() {
  }
  toggleShow(){
    this.show=!this.show;
  }

  showGenerate(){
    this.showGen = !this.showGen;
  }

  public onToggle(): void {
    this.disp = !this.disp;
}
}
