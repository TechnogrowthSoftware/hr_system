import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProbationExtensionletterComponent } from './probation-extensionletter.component';

describe('ProbationExtensionletterComponent', () => {
  let component: ProbationExtensionletterComponent;
  let fixture: ComponentFixture<ProbationExtensionletterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProbationExtensionletterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProbationExtensionletterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
