import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-offer-letter',
  templateUrl: './offer-letter.component.html',
  styleUrls: ['./offer-letter.component.css']
})
export class OfferLetterComponent implements OnInit {
show=true;
  constructor() { }

  ngOnInit() {
  }

  toggleShow(){
    this.show=!this.show;
  }

}
