import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-hr-login',
  templateUrl: './hr-login.component.html',
  styleUrls: ['./hr-login.component.css']
})
export class HrLoginComponent implements OnInit {

  constructor(private route:Router) { }

  ngOnInit() {
  }
  login() {
    this.route.navigate(['/menu/hrdashboard']);
  }
}
