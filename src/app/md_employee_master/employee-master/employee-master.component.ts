import { Component, OnInit } from '@angular/core';
import { IntlService } from '@progress/kendo-angular-intl';

import { FormBuilder, FormGroup } from '@angular/forms';





@Component({
  selector: 'app-employee-master',
  templateUrl: './employee-master.component.html',
  styleUrls: ['./employee-master.component.css']
})
export class EmployeeMasterComponent implements OnInit {
  // Array for selecting Fields
  fields=[
    'Basic Info',
    'Family Details', 
    'Access Card Details', 
    'Bank Details', 
    'PF', 
    'Previous Employment History', 
    'Documents'
  ];

  subFieled=[
    'Employee Name',
    'Employee Code',
    'Date of Birth',
    'Father Name',
    'Spose Name',
    'Contact No',
    'Email Id'
  ];
  
  show = true;
  showDropDown=false;
  showSubDropDown=false;

//  Class Constructor
  constructor(private intl: IntlService) { }
  // Kendo Date
  public onChange(value: Date): void {
    console.log(value);
  }

  ngOnInit() {
    
  }
  // Kendo Date
  public events: string[] = [];
  public value: Date = new Date();


 //showDropDown
 showDropDowns() {
this.showDropDown=!this.showDropDown;
 }
 toggleDropDown() {
   this.showSubDropDown = !this.showSubDropDown;
 }

//  Open DropDown
 openDropDown() {
  this.showDropDown = false;
}

//getSeachValue


// Toggle divs
  toggleShow() {
    this.show = !this.show;
  }

// Save And Continue
selectNext(el){
  el.selectedIndex += 1;
}

}
