import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-salary-master',
  templateUrl: './salary-master.component.html',
  styleUrls: ['./salary-master.component.css']
})
export class SalaryMasterComponent implements OnInit {
 show=true;
  constructor() { }

  ngOnInit() {
  }

  toggleShow(){
    this.show = !this.show;
  }
}
