import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-department-master',
  templateUrl: './department-master.component.html',
  styleUrls: ['./department-master.component.css']
})
export class DepartmentMasterComponent implements OnInit {
show=true;
  constructor() { }

  ngOnInit() {
  }

  toggleShow(){
    this.show=!this.show;
  }

}
