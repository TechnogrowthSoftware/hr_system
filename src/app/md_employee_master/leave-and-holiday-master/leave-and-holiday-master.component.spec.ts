import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveAndHolidayMasterComponent } from './leave-and-holiday-master.component';

describe('LeaveAndHolidayMasterComponent', () => {
  let component: LeaveAndHolidayMasterComponent;
  let fixture: ComponentFixture<LeaveAndHolidayMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaveAndHolidayMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveAndHolidayMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
