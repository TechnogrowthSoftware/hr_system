import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-leave-and-holiday-master',
  templateUrl: './leave-and-holiday-master.component.html',
  styleUrls: ['./leave-and-holiday-master.component.css']
})
export class LeaveAndHolidayMasterComponent implements OnInit {
show = true;
  constructor() { }

  ngOnInit() {
  }
  toggleShow(){
    this.show = !this.show;
  }
}
